package inheritance;

import lombok.Data;

@Data
public abstract class Organisasi {
    private String namaOrganisasi;
    private String jenisOrganisasi;
    public abstract void tugas();
    public abstract void tanggungJawab();
}

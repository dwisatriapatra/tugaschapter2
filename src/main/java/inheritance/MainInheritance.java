package inheritance;

public class MainInheritance {
    public static void main(String[] args) {
        Bem bemUB = new Bem();
        Dpm dpmUB = new Dpm();

        bemUB.setNamaOrganisasi("BEM UB");
        bemUB.setJenisOrganisasi("Eksekutif");

        dpmUB.setNamaOrganisasi("DPM UB");
        dpmUB.setJenisOrganisasi("Legislatif");

        System.out.println(bemUB.getNamaOrganisasi());
        System.out.println(dpmUB.getNamaOrganisasi());
        bemUB.tugas();
        dpmUB.tugas();
    }
}

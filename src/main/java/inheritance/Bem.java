package inheritance;

public final class Bem extends Organisasi{
    @Override
    public void tugas() {
        System.out.println("Tugas terkait eksekutif");
    }

    @Override
    public void tanggungJawab() {
        System.out.println("Tanggung jawab terkait eksekutif");
    }
}

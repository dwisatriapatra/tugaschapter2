package inheritance;

public final class Dpm extends Organisasi{
    @Override
    public void tugas() {
        System.out.println("Tugas terkait legislatif");
    }

    @Override
    public void tanggungJawab() {
        System.out.println("Tanggung jawab terkait legislatif");
    }
}

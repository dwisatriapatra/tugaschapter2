package encapsulation;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class Sekolah {
    // di private agar tidak bisa diubah nilainya secara sembarangan di kelas lain
    private String namaSekolah;
    private String alamatSekolah;
}

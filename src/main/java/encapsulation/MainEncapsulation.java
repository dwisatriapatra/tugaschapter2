package encapsulation;
public class MainEncapsulation {
    public static void main(String[] args) {
        Sekolah sekolahku = new Sekolah();
        //sekolahku.alamatSekolah = "Bandung";  error
        sekolahku.setAlamatSekolah("Bandung barat");
        System.out.println(sekolahku.getAlamatSekolah());
    }
}

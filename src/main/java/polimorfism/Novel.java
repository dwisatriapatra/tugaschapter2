package polimorfism;

public final class Novel extends Buku {
    // polimorfism jenis overriding
    @Override
    public int jumlahHalaman() {
        return 250;
    }

    // polimorfism jenis overloading
    public void printDataNovel(String judul){
        System.out.println("Judul novel: " + judul);
    }
    public void printDataNovel(String judul, String penulis){
        System.out.println("Judul novel: " + judul + ", Penulis: " + penulis);
    }
}

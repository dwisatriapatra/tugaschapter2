package polimorfism;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public abstract class Buku {
    private String jenisKertas;
    public abstract int jumlahHalaman();
}

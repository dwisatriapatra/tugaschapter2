package polimorfism;

public class MainPolimorfism {
    public static void main(String[] args) {
        Novel novelku = new Novel();
        novelku.setJenisKertas("kertas koran");

        System.out.println(novelku.getJenisKertas());
        System.out.println(novelku.jumlahHalaman());
    }
}

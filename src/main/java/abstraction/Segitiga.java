package abstraction;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public final class Segitiga extends BangunDatar implements Warna{
    private int alas, tinggi;
    @Override
    public double hitungLuas() {
        return 0.5 * alas * tinggi;
    }

    @Override
    public String kodeWarna() {
        return "(56, 70, 255)";
    }

    @Override
    public int tingkatKecerahan() {
        return 95;
    }
}

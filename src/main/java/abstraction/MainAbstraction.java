package abstraction;

public class MainAbstraction {
    public static void main(String[] args) {
        Persegi persegiSatu = new Persegi();
        Segitiga segitigaSatu = new Segitiga();

        persegiSatu.setJumlahSisi(4);
        persegiSatu.setPanjangSisi(30);

        segitigaSatu.setJumlahSisi(3);
        segitigaSatu.setAlas(3);
        segitigaSatu.setTinggi(4);

        System.out.println("jumlah sisi persegi satu: " + persegiSatu.getJumlahSisi());
        System.out.println("Luas persegi satu: " + (int)persegiSatu.hitungLuas());

        System.out.println("jumlah sisi segitiga 1: " + segitigaSatu.getJumlahSisi());
        System.out.println("Luas segitiga satu: " + (int)segitigaSatu.hitungLuas());
    }
}

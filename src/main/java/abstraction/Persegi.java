package abstraction;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public final class Persegi extends BangunDatar implements Warna{
    private int panjangSisi;
    @Override
    public double hitungLuas() {
        return panjangSisi * panjangSisi;
    }

    @Override
    public String kodeWarna() {
        return "(21, 31, 400)";
    }

    @Override
    public int tingkatKecerahan() {
        return 85;
    }
}

package abstraction;

import lombok.Data;

@Data
public abstract class BangunDatar {
    private int jumlahSisi;
    public abstract double hitungLuas();
}

package abstraction;

interface Warna {
    String kodeWarna();
    int tingkatKecerahan();
}
